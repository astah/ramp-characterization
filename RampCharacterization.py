#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 10:04:15 2019

@author: astahannesdottir
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.special import erf
import pywt

plt.close('all')

"""
The test data consists of wind speed measurements at 100 m from a light mast 
in Høvsøre. These are 6 examples of high variance events of 30 minute duration.  
The test data has been down sampled from 10 Hz to 1 Hz for code speed and 
simplicity.

"""

wsp1hz = np.load('test_data.npy')

nr = len(wsp1hz)
    
#%% Plot the mirrored DOG1 Wavelet
wgaus1 = pywt.ContinuousWavelet('gaus1')
psi, x0 = wgaus1.wavefun(level=10)

plt.figure()
plt.plot(x0, -psi) #Plottet with minus to mirror the wavelet  
plt.xlim(x0[0], x0[-1])
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')

#%% Normalization of the data before calculating the continuous wavelet transform
wsp1norm = ((wsp1hz.T - wsp1hz.mean(axis=1))/wsp1hz.std(axis=1)).T
scales = np.arange(12, 300)
time = np.arange(0, 1800)

#%% Wavelet transform
wt_co = np.zeros((nr,len(scales),len(time)))
wt_conorm = np.zeros((nr,len(scales),len(time)))
time_comax = np.zeros(nr)
scale_comax = np.zeros(nr)

for i in range(nr):
    wt_co[i], freqs = pywt.cwt(-wsp1norm[i,:], scales, 'gaus1', 1)
    wt_conorm[i] = (wt_co[i].T/scales**(1/2)).T
    ind_sc_comax, time_comax[i] = np.unravel_index(wt_conorm[i].argmax(), \
                            wt_conorm[i].shape)
    scale_comax[i] = scales[ind_sc_comax]

#%% The Ramp function

def ramp(time,ub,ua,tr,dt):
    result = (ub+ua)/2 + (ua-ub)/2*erf((time-tr)/dt)
    return result

#%% 
"""
The data ranges are defined as 3 times the scale (scale_comax) centered around 
the time_comax.
"""
ranges = np.stack((time_comax-1.5*scale_comax,time_comax+1.5*scale_comax),axis=1)
ranges = np.round(ranges).astype(int)
fit_par = np.zeros((nr,4))
par_std_err = np.zeros((nr,4))

for i in range(nr):
    p0 = wsp1hz[i,ranges[i,0]], wsp1hz[i,ranges[i,1]], time_comax[i], 10
    fit_par[i,:], pcov =  curve_fit(ramp, time[ranges[i,0]:ranges[i,1]], \
             wsp1hz[i,ranges[i,0]:ranges[i,1]], p0, bounds=([0,0,0,2],np.inf))
    par_std_err[i,:] = np.sqrt(np.diag(pcov))

"""
The ramp amplitudes may be calculated with: fit_par[:,1] - fit_par[:,0]
The ramp rise times may be calculated with: fit_par[:,3]*3.17
"""
#%% Plots
for i in range(nr):
    plt.figure()
    plt.subplot(2,1,1)
    plt.plot(time,wsp1hz[i,:], label='Wind speed')
    plt.xlim(0,1800)
    plt.plot(time[ranges[i,0]:ranges[i,1]], ramp(time[ranges[i,0]:ranges[i,1]],\
             *fit_par[i]),'r--', label='profile fit')
    plt.legend()
    plt.ylabel('Wsp [m/s]')
    plt.subplot(2,1,2)
    plt.imshow(wt_conorm[i], extent=[0, 1800, scales[-1], scales[0]], \
               cmap='coolwarm', aspect='auto', vmax=abs(wt_conorm).max(), \
               vmin=-abs(wt_conorm).max())  
    plt.plot(time_comax[i], scale_comax[i],'o')
    plt.xlabel('Time [s]')
    plt.ylabel('Scale [s]')
