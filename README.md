# Characterization of extreme wind speed ramps

This repository includes a python script and test data of 6 wind speed ramps. The script performs characterization of the wind speed ramps. For more information of the characterization method read: https://www.wind-energ-sci-discuss.net/wes-2018-68/
